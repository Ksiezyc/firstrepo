'use strict';

angular.
  module('countryList').
  component('countryList', {
    templateUrl: 'country-list/country-list.template.html',
    controller: ['$http', function countryListController($http) {
      var self = this;
      self.orderProp = 'name';
      self.countries = [];

      $http.get('https://restcountries-v1.p.mashape.com/all').then(function(response) {
        self.countries = response.data;
      });
    }]
  });
