'use strict';

angular.
  module('countryApp').
  config(['$locationProvider' ,'$routeProvider',
    function config($locationProvider, $routeProvider) {
      $locationProvider.hashPrefix('!');

      $routeProvider.
        when('/countries', {
          template: '<country-list></country-list>'
        }).
        otherwise('/countries');
    }
  ]);
